<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
      'name' => $faker->name,
      'description' => $faker->text,
      'image' => $faker->image(public_path('images'), 400, 400, '', false),
    ];
});
