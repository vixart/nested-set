<?php

use Illuminate\Database\Seeder;
use Illuminate\Filesystem\Filesystem;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = new Filesystem;
        $file->cleanDirectory(public_path('images'));
        factory(App\Category::class, rand(2, 3))->create()->each(function ($c1) {
            factory(App\Category::class, rand(1, 2))->create(['parent_id' => $c1->id])->each(function ($c2) {
                factory(App\Category::class, rand(1, 2))->create(['parent_id' => $c2->id])->each(function ($c3) {
                    factory(App\Product::class, rand(1, 1))->create(['category_id' => $c3->id]);
                });
                factory(App\Product::class, rand(1, 1))->create(['category_id' => $c2->id]);
            });
            factory(App\Product::class, rand(1, 1))->create(['category_id' => $c1->id]);
        });
    }
}
