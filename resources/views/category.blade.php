<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/bulma.css">
  </head>
  <body>
    <div id="app">
      <div class="columns">
        <div class="column">
          @foreach ($trees as $tree)
              <tree model="{{ $tree }}"></tree>
          @endforeach
        </div>
        <div class="column">
          <products> </products>
        </div>
      </div>
      <!-- <tree-component></tree-component> -->
    </div>
    <script src="/js/app.js"></script>
  </body>
</html>
