
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vuex from 'vuex';
Vue.use(Vuex);

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

import VueResource from 'vue-resource'
Vue.use(VueResource);

import VueRouter from 'vue-router'
Vue.use(VueRouter)
import Tree from './components/TreeComponent.vue'
import Products from './components/ProductsComponent.vue'

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/spa/:id',
      name: 'tree',
      component: Tree
    }
  ],
});

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
// Vue.component('tree-component', require('./components/TreeComponent.vue'));
// Vue.component('item-component', require('./components/ItemComponent.vue'));

const app = new Vue({
    el: '#app',
    components: { Tree, Products },
    router,
    store: new Vuex.Store({
      state: {
        id: ''
      },
      mutations: {
        increment (state) {
          state.count++
        }
      }
    })
});
