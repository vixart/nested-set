<?php
namespace App\Services;
use App\Product;
use App\Category;
use App\Http\Requests\StoreProduct;
use App\Http\Requests\UpdateProduct;
use App\Repositories\Repository;
use Illuminate\Support\Facades\File;

class ProductService {
  protected $model;

  public function __construct(Product $product)
  {
    $this->model = new Repository($product);
  }

  public function index()
  {
    return $this->model->all();
  }

  public function store(StoreProduct $request)
  {
    $image_name = time().'.'.$request->image->getClientOriginalExtension();
    $request->image->move(public_path('images'), $image_name);
    $data = $request->only($this->model->getModel()->fillable);
    $data['image'] = $image_name;
    $product = $this->model->create($data);
    $product->category()->associate($request->category_id);
    $product->save();
    return $product;
  }

  public function show($id)
  {
    return $this->model->show($id);
  }

  public function update(UpdateProduct $request, $id)
  {
    $product = $this->model->show($id);

    $old_image = public_path('images/'.$product->image);
    if (File::exists($old_image)) {
      File::delete($old_image);
    }
    $image_name = time().'.'.$request->image->getClientOriginalExtension();
    $request->image->move(public_path('images'), $image_name);

    $data = $request->only($this->model->getModel()->fillable);
    $data['image'] = $image_name;
    $this->model->update($data, $id);
    $product->category()->associate($request->category_id);
    $product->save();
    return $this->model->show($id);
  }

  public function destroy($id)
  {
    $product = $this->model->show($id);
    $old_image = public_path('images/'.$product->image);
    if (File::exists($old_image)) {
        File::delete($old_image);
    }

    return $this->model->delete($id);
  }
}
?>
