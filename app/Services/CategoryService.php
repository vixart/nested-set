<?php
namespace App\Services;
use App\Category;
use App\Http\Requests\StoreCategory;
use App\Http\Requests\UpdateCategory;
use App\Repositories\Repository;

class CategoryService {
  protected $model;

  public function __construct(Category $category)
  {
    $this->model = new Repository($category);
  }

  public function index()
  {
    return $this->model->all()->toTree();
  }

  public function store(StoreCategory $request)
  {
    return $this->model->create($request->only($this->model->getModel()->fillable));
  }

  public function show($id)
  {
    return $this->model->show($id);
  }

  public function update(UpdateCategory $request, $id)
  {
    $this->model->update($request->only($this->model->getModel()->fillable), $id);
    return $this->model->show($id);
  }

  public function destroy($id)
  {
    return $this->model->delete($id);
  }
}

?>
