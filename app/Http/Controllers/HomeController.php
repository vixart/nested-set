<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class HomeController extends Controller
{
    public function index()
    {
        $trees = Category::get()->toTree();
        return view('category', ['trees' => $trees]);
    }
}
