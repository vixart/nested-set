<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/spa/{any}', 'HomeController@index')->where('any', '.*');
Route::get('/default', function () {
    return view('default');
});
Route::get('/default', 'API\CategoryController@index');
